cask_args appdir: '/Applications'

tap 'caskroom/cask'
tap 'caskroom/versions'
tap 'caskroom/fonts'
tap 'thoughtbot/formulae'
tap 'neovim/neovim'
tap 'ethereum/ethereum'
tap 'coursier/formulas'
tap 'd12frosted/emacs-plus'
tap 'jmhobbs/parrot'
tap 'paritytech/paritytech'
tap 'raggi/ale'
tap 'ocaml/ocaml'
# tap 'railwaycat/emacsmacport'

brew 'rcm'
brew 'mas'

cask 'java'
cask 'java6'
cask 'java8'

# brew 'rust'
# rust is installed in Makefile
# see https://rustup.rs/ to understand how rust is installed

brew 'elixir'
brew 'rebar3'
brew 'python'
brew 'python3'
brew 'flow'
brew 'go'
brew 'gradle'
brew 'ponyc'
brew 'racket'
brew 'scala'
brew 'sbt'
brew 'coursier/formulas/coursier', args: ['HEAD']
brew 'swi-prolog'
brew 'mercury'
brew 'fop'

brew 'sdl2'
brew 'sdl2_image'
brew 'sdl2_gfx'
brew 'sdl2_mixer'
brew 'sdl2_net'
brew 'sdl2_ttf'

brew 'pgcli'
brew 'mycli'

brew 'hg'
brew 'darcs'
brew 'git'
brew 'gitsh'
brew 'git-extras'
brew 'git-lfs'
brew 'ghi'
brew 'hub'
brew 'tmux'
brew 'ctags'
brew 'graphviz'

brew 'contacts'
brew 'tree'
brew 'telnet'
brew 'mosh'
brew 'm-cli'
brew 'terminal-parrot'
brew 'ccat'
brew 'drafter'
brew 'openssl'
brew 'asciinema'
brew 'msmtp'
brew 'mutt'
brew 'coreutils'
brew 'gnutls'
brew 'texinfo'
brew 'ranger'
brew 'slurm'
brew 'gpg'
brew 'ethereum'
brew 'swagger-codegen'
brew 'pev'
brew 'upx'
brew 'openssl'
brew 'capstone'
brew 'libmagic'
brew 'yara'
brew 'exercism'
brew 'djview4'

# https://github.com/BurntSushi/ripgrep
brew 'ripgrep'
# https://github.com/sharkdp/fd
brew 'fd'
# https://github.com/sharkdp/bat
brew 'bat'

# required for mu4e
brew 'openssl-osx-ca'
brew 'offline-imap'
brew 'autoconf-archive'
brew 'mu', args: ['with-emacs']

brew 'zsh'
brew 'zplug'
brew 'fish'

brew 'the_silver_searcher'
brew 'fzf'

brew 'mu'
# NOTE: http://lists.gnu.org/archive/html/bug-gnu-emacs/2018-10/msg00084.html
brew 'emacs-plus', args: ['HEAD', 'with-no-titlebar', 'without-spacemacs-icon', 'with-modern-icon']
brew 'neovim'
brew 'vim', args: ['override-system-vi', 'with-lua', 'with-perl', 'with-python3', 'with-ruby']

# I use htop-vim fork instead
# brew 'htop'

brew 'httpie'
brew 'jq'
brew 'jid'
brew 'cheat'
brew 'sshrc'
brew 'lesspipe', args: ['with-syntax-highlighting']
brew 'cowsay'

brew 'wget', args: ['enable-iri']
brew 'nmap'
brew 'zmap'

brew 'postgresql'
brew 'redis'
brew 'mysql'
brew 'sqlite'
brew 'mongodb'

brew 'llvm'
brew 'purescript'
brew 'stack'
brew 'cabal-install'
brew 'agda'
brew 'ocaml'
brew 'opam'
brew 'octave'
brew 'coq'
brew 'algol68g'
brew 'solidity'

brew 'autoconf'
brew 'automake'
brew 'libtool'
brew 'gettext'
brew 'boost'
# de, en, fr langs are installed by default
brew 'aspell', args: ['with-lang-ru']
brew 'hunspell'
brew 'ispell'
brew 'nasm'
brew 'bochs'
brew 'qemu'
brew 'xorriso'
brew 'gmp'
brew 'cairo'
brew 'mpfr'
brew 'libmpc'
brew 'icu4c'
brew 'gcc'

brew 'jenv'
brew 'rbenv'
brew 'nvm'
brew 'yarn', args: ['without-node']

brew 'kubectl'
brew 'leiningen'
brew 'nginx'

brew 'imagemagick'
brew 'ffmpeg', args: ['with-libvpx', 'with-libvorbis']

brew 'weechat', args: ['with-perl']
brew 'irssi'

cask 'iterm2'
cask 'nvalt'
cask 'insomnia'
cask 'mactex'

cask 'virtualbox'
cask 'vagrant'
cask 'docker'
cask 'kitematic'

cask 'dropbox'
cask 'transmission'

cask 'the-unarchiver'
cask 'slate'
cask 'amethyst'
cask 'showyedge'
cask 'flux'
cask 'macdown'
cask 'qblocker'
cask 'gyazo'
cask 'ngrok'

# cask 'emacs'  # emacs-plus ftw
# cask 'macvim' # vimrc ftw

cask 'visual-studio-code'
cask 'lighttable'
cask 'intellij-idea-ce'

cask 'skype'
cask 'telegram-desktop-dev'
cask 'discord'
cask 'gitter'
cask 'slack-beta'
cask 'screenhero'
cask 'teamviewer'

cask 'kap'
cask 'vlc'
cask 'wireshark'
cask 'tunnelblick'
cask 'gpgtools'
cask 'keybase'
cask 'nylas-mail'

cask 'pgadmin4'
cask 'google-cloud-sdk'

cask 'tor-browser'
cask 'google-chrome'
cask 'google-chrome-canary'
cask 'firefox'
cask 'opera'
cask 'opera-developer'
cask 'mist'
cask 'brave'

cask 'hex-fiend'
cask '0xed'

# quick look plugins
# https://github.com/sindresorhus/quick-look-plugins

cask 'qlcolorcode'
cask 'qlstephen'
cask 'qlmarkdown'
cask 'quicklook-json'
cask 'qlprettypatch'
cask 'quicklook-csv'
cask 'qlimagesize'
cask 'webpquicklook'
cask 'suspicious-package'
cask 'quicklookase'
cask 'qlvideo'

cask 'twitch'

# fonts

cask 'font-inconsolata'
cask 'font-vollkorn'
cask 'font-iosevka'
cask 'font-source-code-pro'
cask 'font-source-sans-pro'
cask 'font-courier-prime'
cask 'font-source-serif-pro'
cask 'font-hack'
cask 'font-fira-code'

cask 'osxfuse' # for dired-avfs (which is required for dired-hacks)
brew 'avfs'    # because it depends on osxfuse

# apps from appstore

mas 'Xcode', id: 497799835
mas 'Helium', id: 1054607607
mas 'Pixen', id: 525180431
mas 'Noizio', id: 928871589
mas 'TweetDeck', id: 485812721
mas 'Monosnap', id: 540348655
mas 'Twitter', id: 409789998

# see README.md for a list of paid apps that I use
